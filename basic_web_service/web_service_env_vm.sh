#!/usr/bin/env bash

source ./functions.sh

set -o nounset

declare vm_name='WordPress_server'
declare nat_name='nasp_cm_co'

check_root

# Create VM
vboxmanage createvm \
	--name $vm_name \
	--ostype 'RedHat_64' \
	--register

# Modify the VM Settings
vboxmanage modifyvm $vm_name \
	--cpus 1 \
	--firmware bios \
	--nic1 natnetwork \
	--nictype1 virtio \
	--nat-network1 "$nat_name" \
	--cableconnected1 on \
	--audio none \
	--boot1 dvd \
	--boot2 disk \
	--memory 2048 \
	--vram 128

# Create IDE Controller
VBoxManage storagectl $vm_name \
	--name 'IDE Controller' \
	--add ide \
	--bootable on

# Create SATA Controller
VBoxManage storagectl $vm_name \
	--name 'SATA Controller' \
	--add sata \
	--bootable on

# Attach the CentOS .iso
vboxmanage storageattach $vm_name \
	--storagectl 'IDE Controller' \
	--port 0 \
	--device 0 \
	--type dvddrive \
	--medium '/home/drew/Downloads/CentOS-7-x86_64-Minimal-1611.iso'

# Attach Guest Additions .iso
VBoxManage storageattach $vm_name \
	--storagectl 'IDE Controller' \
	--port 1 \
	--device 0 \
	--type dvddrive \
	--medium '/usr/share/virtualbox/VBoxGuestAdditions.iso'

# Create HDD
VBoxManage createhd \
	--filename "/home/drew/VirtualBox VMs/$vm_name/$vm_name.vdi" \
	--size 20480

# Attach HDD
VBoxManage storageattach $vm_name \
	--storagectl 'SATA Controller' \
	--port 0 \
	--device 0 \
	--type hdd \
	--medium "/home/drew/VirtualBox VMs/$vm_name/$vm_name.vdi" > /dev/null 2>&1

vboxmanage startvm $vm_name

