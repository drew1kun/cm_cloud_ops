#!/usr/bin/env bash

source ./functions.sh

set -o nounset
declare nat_name="nasp_cm_co"
declare -a ports=('50080' '50022' '50443')
declare zone='public'

check_root

vboxmanage natnetwork add --netname "${nat_name}" --network "192.168.254.0/24" --dhcp off

for port in "${ports[@]}"; do
	sudo firewall-cmd --zone=$zone --add-port=$port/tcp --permanent
done

vboxmanage natnetwork modify --netname nasp_cm_co --port-forward-4 "ssh:tcp:[]:50022:[192.168.254.10]:22"
vboxmanage natnetwork modify --netname nasp_cm_co --port-forward-4 "http:tcp:[]:50080:[192.168.254.10]:80"
vboxmanage natnetwork modify --netname nasp_cm_co --port-forward-4 "https:tcp:[]:50443:[192.168.254.10]:443"

sudo systemctl restart firewalld.service

