#!/usr/bin/env bash

#-----------------COLORIZE OUTPUT-------------------
function print_good() {
    echo -e "\x1B[01;32m[*]\x1B[0m $1"
}

function print_error() {
    echo -e "\x1B[01;31m[*]\x1B[0m $1"
}

function print_status() {
    echo -e "\x1B[01;34m[*]\x1B[0m $1"
}
#---------------------------------------------------

#--------------------CHECK ROOT---------------------
function check_root() {
	if [[ "$EUID" -eq 0 ]]; then
		print_error "ERROR: Sorry, don't run this as root"
	exit 1
	fi
}
#---------------------------------------------------
