#!/usr/bin/env bash

source ./functions.sh

set -o nounset

declare -a ports=('80' '22' '443')

check_root

sudo yum update -y
sudo yum install -y \
	@core \
	@base \
	epel-release \
	vim \
	git \
	tcpdump \
	nmap-ncat

mkdir /home/admin/.ssh
cat > /home/admin/.ssh/authorized_keys <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCu25cbwUSf2jqveyo/jOle1U2c4V0VXgKYMS9G/374TLxcslzPp2rvPSXYiQIibVSqBv/thvxs8iRm9uLNtd3dwD8Npb/RfXd/I0upoMdMSj/1cDQoY/Rype/JLlaBCdv9UIZeJaur/Ddr0ZdBS7ftMmrOow3A4Tv6cejC+D/wMHr2HVi7lb0zS8vewhhCQSjbx6t+2MxU8c7xfEy944abc6AIHIixJjVo0ETivC9+GPQopF7fWFfrUuErf+1CRerTX3MvsSWSVvdzfvjqnDkW7BAQUsYaWdh6ladXBkxua32UCiqXNwusmXzyeSCVNh8Zt+yi1yT3ZQvHZlW4YWehMQGKxCSLJkkPCcITCkX4l02cs2OMo6Fd5bwggdoXRv1BY9o2/3FXHdYry+oampOyORUYijo6hUs7BbcEUlKMp+LCdr+vOwAjlvKZ5NfgfOxUVAvwcO89fSteYSmd5i6+VNVjBpytXNshMLZA9XZN6fBuYYsL4rf6IWvbWbsrgRzcmas4lcR+UB4SkPTVPAqIiQ0sYENwT03g2wXDHjEdLEVjDnDi9ib8hnl/J1ZeAbVFjFKN8hvP6VCe1tBoWeHmxoDKRsF85dCYpVCaqTi0B4Mbs78Ew0w9bh7GYVSgRkJahXDu9qUOAyuuE0WQRgDvCtduIygFpHNdiX3FxrK7IQ== nasp19_admin
EOF
chown -R admin /home/admin/.ssh
chmod 600 /home/admin/.ssh/*

for port in "${ports[@]}"; do
	sudo firewall-cmd --zone=public --add-port=$port/tcp --permanent
done

sudo yum install -y \
	nginx \
	mariadb \
	mariadb-server \
	php \
	php-mysql \
	php-fpm

#=============================NGINX SETUP===============================
sudo systemctl start nginx
sudo systemctl enable nginx
systemctl status nginx

#=========================== MARIADB SETUP==============================
sudo systemctl start mariadb
systemctl status mariadb
mysql -u root < mariadb_security_config.sql
sudo systemctl enable mariadb

#===============================PHP SETUP===============================
sudo sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php.ini
sudo sed -i '/^listen =/ s/^.*$/listen = \/var\/run\/php-fpm\/php-fpm.sock/' /etc/php-fpm.d/www.conf
sudo sed -i '/^listen.owner = nobody/ s/^;//' /etc/php-fpm.d/www.conf
sudo sed -i '/^listen.group = nobody/ s/^;//' /etc/php-fpm.d/www.conf
sudo sed -i 's/^user = .*$/user = nginx/' /etc/php-fpm.d/www.conf
sudo sed -i 's/^group = .*$/group = nginx/' /etc/php-fpm.d/www.conf

sudo systemctl start php-fpm
sudo systemctl enable php-fpm

# remove a file and copy the correct one:
sudo rm -rf /etc/nginx/nginx.conf
sudo cp etc/nginx/nginx.conf /etc/nginx/nginx.conf
# Verify php working and check it's info (version etc.)
#echo "<?php phpinfo(); ?>" > /usr/share/nginx/html/info.php
sudo systemctl restart nginx

#============================WORDPRESS SETUP=============================
mysql -u root < wp_mariadb_config.sql

# Verify user creation:
#mysql -u root -p -e "SELECT user FROM mysql.user;"
# Verify database creation
#mysql -u root -p -e "SHOW DATABASES;"

# Download wordpress
wget http://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz

# Create a wordpress configuration:
cp wordpress/wp-config-sample.php wordpress/wp-config.php
sed -i '/DB_NAME/ s/database_name_here/wordpress/' wordpress/wp-config.php
sed -i '/DB_USER/ s/username_here/wordpress_user/' wordpress/wp-config.php
sed -i '/DB_PASSWORD/ s/password_here/nasp19/' wordpress/wp-config.php

# Copy the WordPress source to the nginx document root using rsync:
sudo rsync -avP wordpress/ /usr/share/nginx/html/
echo "<?php phpinfo(); ?>" > /usr/share/nginx/html/info.php
rm -rf wordpress latest.tar.gz

# Make a directory for uploads:
sudo mkdir /usr/share/nginx/html/wp-content/uploads
# Set the permissions on the WordPress source:
sudo chown -R admin:nginx /usr/share/nginx/html/*

#======================= Vbox Guest Additions==========================
# MUST BE RUN AFTER REBOOT
# Installing pre-requisities
#sudo yum -y install kernel-devel kernel-headers dkms gcc gcc-c++ kexec-tools

# Creating mount point, mounting, and installing VirtualBox Guest Additions
# Assumes that the virtualbox guest additions CD is in /dev/cdrom
#mkdir vbox_cd
#sudo mount /dev/cdrom ./vbox_cd
#sudo ./vbox_cd/VBoxLinuxAdditions.run
#sudo umount ./vbox_cd
#rmdir ./vbox_cd

